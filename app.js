var express = require("express"),
    mongoose = require("mongoose"),
    bodyParser = require("body-parser"),
    passport = require("passport"),
    localStrategy = require("passport-local"),
    passportLocalMongoose = require("passport-local-mongoose"),
    User = require("./models/user")

mongoose.connect("mongodb://localhost/auth_demo", { useNewUrlParser: true });

var app = express();
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(require("express-session")({
    secret: "uzair is developer",
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

passport.use(new localStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());


app.use(function(req, res, next) {
    res.locals.currentUser = req.user;
    next();
});

app.get("/", function(req, res) {
    res.render("home");
});


app.get("/secret", isLoggedIn, function(req, res) {
    res.render("secret");
});


//*************
//AUTH routes
//*************


//Show SignUp form
app.get("/register", function(req, res) {
    res.render("register");
});

//handling user signup
app.post("/register", function(req, res) {

    var newUser = new User({ username: req.body.username });
    newUser.counter = 1;

    User.register(newUser, req.body.password, function(err, user) {
        if (err) {
            console.log(err);
            return res.redirect("/register");
        }

        passport.authenticate("local")(req, res, function() {
            res.redirect("/secret");
        });
    });
});

//***********************
//Login routes
//***********************

//Show login form
app.get("/login", function(req, res) {
    res.render("login");
});

//handling user login
app.post("/login", passport.authenticate("local"),
    //     successRedirect: "/secret",
    //     failureRedirect: "/login",
    // }),
    function(req, res) {
        User.findOneAndUpdate({ "username": req.user.username }, { $inc: { "counter": 1 } }, function(err, updatedCounter) {
            if (err) {
                console.log(err);
            }
            else {
                console.log(req.user.username);
                console.log(updatedCounter);
                res.redirect("/secret");
            }
        });
    });

//**************
//logout routes
//**************

app.get("/logout", function(req, res) {
    req.logout();
    res.redirect("/");
})


function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect("/login");
}

app.listen(process.env.PORT, process.env.IP, function() {
    console.log("app started");
});
